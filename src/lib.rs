#![doc = include_str!("../README.md")]

use cursive_core::utils::span::{SpannedStr, SpannedString};
use std::str::Chars;
use unicode_width::UnicodeWidthStr;

mod views;
mod funcs;
mod macros;

pub use views::*;
pub use funcs::*;

#[cfg(test)]
mod tests;

// cursive reexport for the macros
#[doc(hidden)]
pub use cursive_core as siv;

/// [`SpannedString`]: cursive_core::utils::span::SpannedString
/// [`SpannedStr`]: cursive_core::utils::span::SpannedStr
/// [`StyledString`]: cursive_core::utils::markup::StyledString
///
/// Extension trait for [`SpannedString`] and [`SpannedStr`]
///
/// These methods might be useful when working with [`StyledString`]s
pub trait SpannedStrExt<T>: Clone {
    /// The amount of UTF-8 characters in the source string
    fn char_len(&self) -> usize;

    /// Iterate over the characters in the source string
    ///
    /// Disclaimer: some special characters are actually multiple characters
    /// (like accented letters)
    fn chars(&self) -> Chars;

    /// Convert the [`SpannedStr`] back to a [`SpannedString`]
    fn to_spanned_string(&self) -> SpannedString<T>;

    /// Creates a [`SpannedStr`] reference from a [`SpannedString`]
    fn as_spanned_str(&self) -> SpannedStr<T>;
}

impl<T: Clone> SpannedStrExt<T> for SpannedString<T> {
    fn char_len(&self) -> usize { self.source().width() }
    fn chars(&self) -> Chars { self.source().chars() }
    fn to_spanned_string(&self) -> SpannedString<T> { self.clone() }
    fn as_spanned_str(&self) -> SpannedStr<T> { SpannedStr::new(self.source(), self.spans_raw()) }
}

impl<'a, T: Clone> SpannedStrExt<T> for SpannedStr<'a, T> {
    fn char_len(&self) -> usize { self.source().width() }
    fn chars(&self) -> Chars { self.source().chars() }
    fn to_spanned_string(&self) -> SpannedString<T> { SpannedString::with_spans(self.source(), self.spans_raw().to_vec()) }
    fn as_spanned_str(&self) -> SpannedStr<T> { SpannedStr::new(self.source(), self.spans_raw()) }
}