use crate::*;
use std::{
    env, thread,
    time::Duration,
    sync::Mutex
};
use cursive::{
    Cursive,
    views::{
        Dialog,
        TextView,
        EditView
    },
    theme::{BaseColor, Color, ColorStyle},
    utils::markup::StyledString,
    event::Event,
    traits::Nameable
};

#[cfg(feature = "tabs")]
use cursive::{
    event::Key,
    views::{Button, ResizedView},
    align::HAlign,
};

#[cfg(any(feature = "tabs", feature = "advanced"))]
use cursive::traits::Scrollable;

#[cfg(feature = "advanced")]
use cursive::{
    views::ViewRef,
    traits::Resizable
};

static RUN: Mutex<()> = Mutex::new(());

#[cfg(feature = "tabs")]
static COOF_INFO: &str =
    "Coronavirus disease 2019 (COVID-19) is a \
    contagious disease caused by a virus, the \
    severe acute respiratory syndrome coronavirus 2 \
    SARS-CoV-2). The first known case was identified in Wuhan, \
    China, in December 2019. The disease quickly spread worldwide, \
    resulting in the COVID-19 pandemic.\n\
    \n\
    Source: https://en.wikipedia.org/wiki/COVID-19";

// status bar test
#[test]
fn statbar() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_fps(30);
    root.set_theme(better_theme());
    root.add_layer(
        vlayout!(
            Dialog::text("status view test")
                .button("Quit", Cursive::quit)
                .button("Yes", |root| {
                    let mut status = root.find_name::<StatusView>("status").unwrap();
                    status.info("yes");
                })
                .button("No", |root| {
                    let mut status = root.find_name::<StatusView>("status").unwrap();
                    status.report_error("Error: no");
                })
                .button("Launch", |root| {
                    let error: Result<&str, &str> = Err("Error: Houston, we have a problem!");
                    let mut status = root.find_name::<StatusView>("status").unwrap();
                    report_error!(status, error);
                })
                .button("Test", |root| {
                    let warn_style = ColorStyle::from(Color::Light(BaseColor::Yellow));
                    let mut status = root.find_name::<StatusView>("status").unwrap();
                    status.set_label(StyledString::styled(format!("Warning! {} has tested positive for the coof!", env::var("USER").unwrap()), warn_style))
                })
                .title("Coof Testing"),
            StatusView::new().label("Application Status").with_name("status")
        )
    );
    root.run();
    drop(runlock);
}

// select_view! macro test 1
#[test]
fn svmacro1() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    let sv = select_view! {
        "Omicron" => 0,
        "Omicron XBB" => 1,
        "Delta" => 2,
        "Alpha" => 3,
        "Iota" => 4
    }.on_submit(|r, _| r.quit());
    root.add_layer(c_focus!(Dialog::around(sv).title("Coof Variants")));
    root.run();
    drop(runlock);
}

// select_view! macro test 2
#[test]
fn svmacro2() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    let sv = select_view! {
        "yes",
        "no"
    }.on_submit(|r, _: &str| r.quit());
    root.add_layer(Dialog::around(sv).title("Coof Testing"));
    root.run();
    drop(runlock);
}

// info dialog test and styled edit view test
#[test]
fn info_dialog() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    root.add_layer(
        Dialog::around(styled_editview("coof", "edit", false))
            .button("Info", |root| {
                let edit_view = root.find_name::<EditView>("edit").unwrap();
                root.add_layer(crate::info_dialog("Info", edit_view.get_content().as_str()));
            })
            .button("Quit", |root| {
                root.add_layer(confirm_dialog("Are you sure?", "Exit?", Cursive::quit));
            })
            .title("Coof Testing")
    );
    root.run();
    drop(runlock);
}

// settings! macro test
#[test]
fn settings1() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    root.set_fps(30);
    root.add_layer(
        settings_cb!(
            "Coof Testing",
            "Test",
            Cursive::quit,
            TextView::new("Variant:"),
            styled_editview("delta", "", false)
        )
    );
    root.run();
    drop(runlock);
}

#[test]
fn settings2() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    root.add_layer(
        settings!(
            "Coof Testing",
            Cursive::quit,
            TextView::new("Variant:"),
            styled_editview("delta", "", false)
        )
    );
    root.run();
    drop(runlock);
}

#[test]
fn load_resource() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());

    crate::load_resource(&mut root,
        "Please Wait...", "Performing coof test...",
        || {
            thread::sleep(Duration::from_secs(5));
            3 + 4
        },
        |root, val| {
            assert_eq!(val, 7);
            root.quit()
        }
    );

    root.run();
    drop(runlock);
}

#[test]
fn loadanimation() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    let anim = LoadingAnimation::new("Loading Dummy Number...", || {
        thread::sleep(Duration::from_secs(5));
        6 * 2
    });
    root.add_layer(Dialog::around(anim.with_name("loader")).title("Loading...").with_name("dialog"));
    root.set_fps(30);

    root.set_global_callback(Event::Refresh, |root| {
        let mut loader = root.find_name::<LoadingAnimation<i32>>("loader").unwrap();
        if loader.is_done() {
            let ret_val = loader.finish().unwrap();
            assert_eq!(ret_val, 12);
            root.quit()
        }
    });
    root.run();
    drop(runlock);
}

// labled checkbox test
#[test]
fn labeled_cbox() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());

    root.add_layer(
        Dialog::around(
            labeled_checkbox("Coof positive", "box", false)
        )
        .button("Quit", Cursive::quit)
        .title("Coof Testing")
    );
    root.run();

    drop(runlock);
}

// checkbox option test
#[test]
fn cbox_option() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    root.add_layer(
        Dialog::around(
            vlayout!(
                labeled_checkbox_cb("Coof \"vaccinated\"", "box", false, |root, checked| {
                    let mut card = root.find_name::<TextView>("card").unwrap();
                    if checked {
                        card.set_content("May I see your \"vaccine\" card?")
                    }
                    else {
                        card.set_content("")
                    }
                }),
                TextView::new("").with_name("card")
            )
        )
            .button("Quit", |root| {
                if !get_checkbox_option(root, "box") {
                    let style = ColorStyle::from(Color::Light(BaseColor::Red));
                    root.add_layer(crate::info_dialog("Warning!",
                        StyledString::styled(format!("{} is not \"vaccinated\" for the coof!", env::var("USER").unwrap()), style))
                    );
                }
                else {
                    root.quit();
                }
            })
            .title("Coof \"Vaccinations\"")
    );
    root.run();
    drop(runlock);
}

#[test]
fn spacers_and_dividers() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_theme(better_theme());
    root.add_fullscreen_layer(
        Dialog::around(
            vlayout!(
                hlayout!(
                    TextView::new("fixed"),
                    fixed_hspacer(7),
                    TextView::new("horizontal spacer")
                ),
                VDivider::new(),
                hlayout!(
                    TextView::new("filled"),
                    filled_hspacer(),
                    TextView::new("horizontal spacer")
                ),
                VDivider::fixed(7),
                hlayout!(
                    vlayout!(
                        TextView::new("fixed"),
                        fixed_vspacer(7),
                        TextView::new("vertical spacer")
                    ),
                    HDivider::new(),
                    vlayout!(
                        TextView::new("filled"),
                        filled_vspacer(),
                        TextView::new("vertical spacer")
                    ),
                    HDivider::fixed(7)
                )
            )
        )
            .title("Spacer and divider test")
            .button("Quit", Cursive::quit)
    );

    root.run();
    drop(runlock);
}

// image view test
#[cfg(feature = "image_view")]
#[test]
fn image_view() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_autorefresh(true);
    let mut coof = ImageView::new((50, 25));
    root.set_theme(better_theme());
    coof.set_image("coof.png");
    let path_fmt = format!("Path: {}", coof.get_path().display());
    root.add_layer(
        Dialog::around(
            vlayout!(
                coof,
                TextView::new(path_fmt)
            )
        )
            .button("Quit", Cursive::quit)
            .title("The Coof Virus")
    );

    root.run();
    drop(runlock);
}

// tab dialog test
#[cfg(feature = "tabs")]
#[test]
fn tabs() {
    let runlock = RUN.lock().unwrap();
    let nums: String = (0..=144).map(|num| format!("{num}\n")).collect();
    let mut root = cursive::default();
    root.set_autohide_menu(false);
    root.add_global_callback(Key::Up, |s| s.select_menubar());
    root.menubar()
        .add_leaf("Yes", |_| { })
        .add_leaf("No", |_| { });

    let tabs = tabs! {
        "Coof Shots" => vlayout!(
            labeled_checkbox_cb("Coof \"vaccinated\"", "box", false, |root, checked| {
                let mut card = root.find_name::<TextView>("card").unwrap();
                if checked { card.set_content("May I see your \"vaccine\" card?") }
                else { card.set_content("") }
            }),
            TextView::new("").with_name("card"),
            hlayout!(
                Button::new("Change Title", |root| {
                    let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
                    let cur_id = tabs.cur_id().unwrap();
                    if let Some(cur_title) = tabs.cur_title() {
                        if cur_title == "Coof Shots" { tabs.set_title(cur_id, "COVID-19 \"Vaccine\""); }
                        else { tabs.set_title(cur_id, "Coof Shots"); }
                    }
                }),
                fixed_hspacer(1),
                Button::new("Title Alignment", |root| {
                    let align_sv = select_view! {
                        "Left" => HAlign::Left,
                        "Center" => HAlign::Center,
                        "Right" => HAlign::Right
                    }
                        .on_submit(|root, align| {
                            root.pop_layer();
                            let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
                            tabs.set_title_align(*align);
                        });
                    root.add_layer(Dialog::around(align_sv).title("Title Alignment"));
                }),
                fixed_hspacer(1),
                Button::new("Button Alignment", |root| {
                    let align_sv = select_view! {
                        "Left" => HAlign::Left,
                        "Center" => HAlign::Center,
                        "Right" => HAlign::Right
                    }
                        .on_submit(|root, align| {
                            root.pop_layer();
                            let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
                            tabs.set_button_align(*align);
                        });
                    root.add_layer(Dialog::around(align_sv).title("Button Alignment"));
                })
            )
        ),
        "Coof Testing" => ResizedView::with_full_screen(
            vlayout!(
                styled_editview("coof", "edit", false),
                TextView::new(format!("{} has tested positive for the coof!", env::var("USER").expect("What is your name again?")))
            )
        ),

        "The Coof Virus" => TextView::new(COOF_INFO),
        "Numbers" => TextView::new(format!("{nums}This line should be visible!")).scrollable()
    }
        .button("Lots of new tabs", |root| {
            let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
            for _ in 0..5 {
                tabs.add_tab("The Coof Virus", ResizedView::with_full_screen(TextView::new(COOF_INFO)));
            }
        })
        .button("New Tab", |root| {
            let mut tabs = root.find_name::<TabDialog>("tabs").unwrap();
            tabs.add_tab("The Coof Virus", ResizedView::with_full_screen(TextView::new(COOF_INFO)));
        })
        .dismiss_button("Quit")
        .close_button(true)
        .next_prev_buttons(true);

    root.add_fullscreen_layer(
        vlayout!(
            tabs.with_name("tabs"),
            Dialog::text("Face Diaper required for entry!"),
            StatusView::new().label("Application Status")
        )
    );
    root.set_theme(better_theme());
    root.run();
    drop(runlock);
}

// tab layer test
#[cfg(feature = "tabs")]
#[test]
fn tab_layer() {
    let nums: String = (0..=144).map(|num| format!("{num}\n")).collect();
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    root.set_autohide_menu(false);
    root.add_global_callback(Key::Up, |s| s.select_menubar());
    root.menubar()
        .add_leaf("Yes", |_| { })
        .add_leaf("No", |_| { });

    let tabs = tab_layer! {
        "Coof Shots" => vlayout!(
            labeled_checkbox_cb("Coof \"vaccinated\"", "box", false, |root, checked| {
                let mut card = root.find_name::<TextView>("card").unwrap();
                if checked { card.set_content("May I see your \"vaccine\" card?") }
                else { card.set_content("") }
            }),
            TextView::new("").with_name("card"),
            hlayout!(
                Button::new("Change Title", |root| {
                    let mut tabs = root.find_name::<TabLayer>("tabs").unwrap();
                    let cur_id = tabs.cur_id().unwrap();
                    let cur_title = tabs.cur_title();
                    if cur_title == "Coof Shots" { tabs.set_title(cur_id, "COVID-19 \"Vaccine\""); }
                    else { tabs.set_title(cur_id, "Coof Shots"); }
                }),
                fixed_hspacer(1),
                Button::new("Title Alignment", |root| {
                    let align_sv = select_view! {
                        "Left" => HAlign::Left,
                        "Center" => HAlign::Center,
                        "Right" => HAlign::Right
                    }
                        .on_submit(|root, align| {
                            root.pop_layer();
                            let mut tabs = root.find_name::<TabLayer>("tabs").unwrap();
                            tabs.set_title_align(*align);
                        });
                    root.add_layer(Dialog::around(align_sv).title("Title Alignment"));
                }),
                fixed_hspacer(1),
                Button::new("Button Alignment", |root| {
                    let align_sv = select_view! {
                        "Left" => HAlign::Left,
                        "Center" => HAlign::Center,
                        "Right" => HAlign::Right
                    }
                        .on_submit(|root, align| {
                            root.pop_layer();
                            let mut tabs = root.find_name::<TabLayer>("tabs").unwrap();
                            tabs.set_button_align(*align);
                        });
                    root.add_layer(Dialog::around(align_sv).title("Button Alignment"));
                })
            )
        ),
        "Coof Testing" => vlayout!(
            styled_editview("coof", "edit", false),
            TextView::new(format!("{} has tested positive for the coof!", env::var("USER").expect("What is your name again?")))
        ),

        "The Coof Virus" => TextView::new(COOF_INFO),
        "Numbers" => TextView::new(format!("{nums}This line should be visible!")).scrollable()
    }
        .button("Lots of new tabs", |root| {
            let mut tabs = root.find_name::<TabLayer>("tabs").unwrap();
            for _ in 0..5 {
                tabs.add_tab("The Coof Virus", TextView::new(COOF_INFO));
            }
        })
        .button("New Tab", |root| {
            let mut tabs = root.find_name::<TabLayer>("tabs").unwrap();
            tabs.add_tab("The Coof Virus", TextView::new(COOF_INFO));
        })
        .dismiss_button("Quit")
        .close_button(true)
        .next_prev_buttons(true);

    root.add_fullscreen_layer(
        vlayout!(
            tabs.with_name("tabs"),
            Dialog::text("Face Diaper required for entry!"),
            StatusView::new().label("Application Status")
        )
    );
    root.set_theme(better_theme());
    root.run();
    drop(runlock);
}

// advanced views test
#[cfg(feature = "advanced")]
#[test]
fn advanced() {
    let runlock = RUN.lock().unwrap();
    let mut root = cursive::default();
    let red_text = StyledString::styled("red", ColorStyle::from(Color::Light(BaseColor::Red)));
    let disabled = StyledString::styled("yes", ColorStyle::from(Color::Light(BaseColor::Red)));
    let yellow_text = StyledString::styled("yellow", ColorStyle::from(Color::Light(BaseColor::Yellow)));
    let blue_text = StyledString::styled("blue", ColorStyle::from(Color::Light(BaseColor::Blue)));
    let bracketed = StyledString::styled("bracketed button", ColorStyle::from(Color::from_256colors(166)));

    let mut multiline_text = StyledString::plain("Multiple ");
    multiline_text.append_styled("lines\n", ColorStyle::from(BaseColor::Red.light()));
    multiline_text.append_styled("and ", ColorStyle::from(BaseColor::Green.light()));
    multiline_text.append_styled("colors!", ColorStyle::from(BaseColor::Cyan.light()));

    let mut multiline2 = StyledString::plain("Multiple ");
    multiline2.append_styled("lines\n", ColorStyle::from(BaseColor::Red));
    multiline2.append_styled("and ", ColorStyle::from(BaseColor::Green));
    multiline2.append_styled("colors ", ColorStyle::from(BaseColor::Cyan));
    multiline2.append_styled("with\nanother line!", ColorStyle::from(Color::from_256colors(166)));
    multiline2.append("\nyes no");

    let mut bracketed_multiline = StyledString::plain("Multiple ");
    bracketed_multiline.append_styled("lines\n", ColorStyle::from(BaseColor::Red));
    bracketed_multiline.append_styled("and ", ColorStyle::from(BaseColor::Green));
    bracketed_multiline.append_styled("colors ", ColorStyle::from(BaseColor::Cyan));
    bracketed_multiline.append_styled("with\nbrackets!", ColorStyle::from(Color::from_256colors(166)));
    bracketed_multiline.append(" yes no");

    let adv_sv = adv_select_view! {
        red_text.clone() => 0usize,
        yellow_text.clone() => 1,
        multiline_text.clone() => 2,
        blue_text.clone() => 3,
        multiline_text.clone() => 4,
        multiline2 => 5,
        "This is a really long line that should wrap" => 6
    }
        .on_select(|root, selected| {
            let mut op_view: ViewRef<TextView> = root.find_name("op_view").unwrap();
            op_view.set_content(format!("Selected option: {selected}"));
        })
        .on_submit(|root, selected| {
            root.add_layer(crate::info_dialog("Selected Option", format!("Selected option: {selected}")))
        });

    let callback = |root: &mut Cursive| root.add_layer(crate::info_dialog("Yes", "yes"));

    root.set_theme(better_theme());
    root.add_layer(
        c_focus!(
            Dialog::around(
                hlayout!(
                    vlayout!(
                        TextView::new("Advanced Buttons\n "),
                        AdvancedButton::new(red_text, callback),
                        AdvancedButton::new(yellow_text, |root| {
                            let warn_style = ColorStyle::from(Color::Light(BaseColor::Yellow));
                            let warning = StyledString::styled(format!("{} has tested positive for the coof!", env::var("USER").unwrap()), warn_style);
                            root.add_layer(crate::info_dialog("Warning!", warning))
                        }),
                        AdvancedButton::new(bracketed, callback).brackets(),
                        AdvancedButton::new(disabled, |_| { }).disabled(),
                        AdvancedButton::new(blue_text, callback),
                        AdvancedButton::new(multiline_text, callback),
                        AdvancedButton::new(bracketed_multiline, callback).brackets(),
                        AdvancedButton::new("disabled", |_| { }).disabled(),
                        AdvancedButton::new_with_data("Times clicked: 0", 0usize, |root| {
                            let mut button: ViewRef<AdvancedButton<usize>> = root.find_name("button").unwrap();
                            *(button.get_data_mut()) += 1;
                            let times = *(button.get_data());
                            button.set_title(format!("Times clicked: {times}"));
                        }).with_name("button"),
                        AdvancedButton::new("This is a really long line that should wrap", callback).fixed_width(15)
                    ).scrollable(),
                    HDivider::new(),
                    vlayout!(
                        TextView::new("Advanced Select View\n "),
                        adv_sv.with_name("adv_sv")
                            .scrollable()
                            .max_height(4),
                        TextView::new(" "),
                        labeled_checkbox_cb("Enabled", "", true, |root, enabled| {
                            let mut adv_sv: ViewRef<AdvancedSelectView<usize>> = root.find_name("adv_sv").unwrap();
                            adv_sv.set_enabled(enabled);
                        }),
                        labeled_checkbox_cb("Autojump", "", false, |root, autojump| {
                            let mut adv_sv: ViewRef<AdvancedSelectView<usize>> = root.find_name("adv_sv").unwrap();
                            adv_sv.set_autojump(autojump);
                        }),
                        labeled_checkbox_cb("Popup mode", "", false, |root, popup| {
                            let mut adv_sv: ViewRef<AdvancedSelectView<usize>> = root.find_name("adv_sv").unwrap();
                            adv_sv.set_popup(popup);
                        }),
                        TextView::new("Selected option: 0").with_name("op_view")
                    )
                )
            )
                .title("advanced views test")
                .button("Quit", Cursive::quit)
        )
            .max_size((40, 20))
    );

    root.run();
    drop(runlock);
}