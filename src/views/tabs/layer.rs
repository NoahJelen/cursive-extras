use crossbeam_channel::{bounded, Receiver, Sender};
use cursive_core::{
    Cursive, Printer, Vec2, View,
    align::HAlign,
    traits::{Scrollable, With, Resizable},
    direction::{Absolute, Direction},
    event::{
        AnyCb, Event,
        EventResult, Key,
        MouseButton, MouseEvent
    },
    view::{
        CannotFocus,
        Position,
        Selector,
        ViewNotFound
    },
    views::{
        BoxedView,
        LayerPosition,
        SelectView,
        Dialog,
        OnEventView
    }
};
use rust_utils::chainable;
use crate::c_focus;
use super::{
    TitleButtonType,
    TabContainer,
    TabFocus,
    TabIter,
    TabIterMut,
    ViewButton,
    ButtonAction,
    TitleButton
};

/// A view similar to `TabDialog` but it fills up all the available space
///
/// Best used with `Cursive::add_fullscreen_layer()`
pub struct TabLayer {
    // the underlying tab view
    tabs: TabContainer,

    // buttons on the title bar
    shown_title_buttons: Vec<TitleButton>,

    // all the title bar buttons
    title_buttons: Vec<TitleButton>,

    // alignment of the title bar
    title_align: HAlign,

    // dialog buttons
    buttons: Vec<ViewButton>,

    // button alignment
    button_align: HAlign,

    // what is being focused?
    focus: TabFocus,

    // channel to communicate with tab overflow menu
    id_snd: Sender<usize>,
    id_rcv: Receiver<usize>
}

impl TabLayer {
    /// Create a new `TabLayer`
    #[must_use]
    pub fn new() -> TabLayer {
        let (id_snd, id_rcv) = bounded(1);
        TabLayer {
            tabs: TabContainer::new(),
            title_buttons: vec![],
            shown_title_buttons: vec![],
            title_align: HAlign::Center,
            buttons: vec![],
            button_align: HAlign::Left,
            focus: TabFocus::Content,
            id_snd,
            id_rcv
        }
    }

    /// Add a new button to the view
    #[chainable]
    pub fn add_button<F: Fn(&mut Cursive) + Send + Sync + 'static>(&mut self, text: &str, func: F) {
        let new_button = ViewButton::from_fn(text, func);
        if self.has_next_prev_buttons() {
            let last_idx = self.buttons.len() - 2;
            self.buttons.insert(last_idx, new_button);
        }
        else { self.buttons.push(new_button) }
    }

    /// Add a button to close the view
    #[chainable]
    pub fn add_dismiss_button(&mut self, text: &str) {
        let new_button = ViewButton::from_fn(text, |r| {
            if r.screen().len() <= 1 { r.quit(); }
            else { r.pop_layer(); }
        });
        if self.has_next_prev_buttons() {
            let last_idx = self.buttons.len() - 2;
            self.buttons.insert(last_idx, new_button);
        }
        else { self.buttons.push(new_button) }
    }

    /// Add buttons to cycle through the tabs
    #[chainable]
    pub fn add_next_prev_buttons(&mut self, show: bool) {
        if show {
            self.buttons.insert(0, ViewButton::new("Previous", ButtonAction::Prev));
            self.buttons.push(ViewButton::new("Next", ButtonAction::Next));
        }
        else if self.has_next_prev_buttons() {
            self.buttons.remove(0);
            self.buttons.pop();
        }
    }

    /// Add a button to remove the current tab
    #[chainable]
    pub fn add_close_button(&mut self, show: bool) {
        if show {
            let new_button = ViewButton::new("Close", ButtonAction::Close);
            if self.has_next_prev_buttons() {
                let last_idx = self.buttons.len() - 2;
                self.buttons.insert(last_idx, new_button);
            }
            else { self.buttons.push(new_button) }
        }
        else {
            self.buttons.retain(|b| matches!(b.action, ButtonAction::Close));
        }
    }

    /// Set the alignment of the buttons
    #[chainable]
    pub fn set_button_align(&mut self, align: HAlign) { self.button_align = align; }

    /// Set the alignment of the buttons
    #[chainable]
    pub fn set_title_align(&mut self, align: HAlign) { self.title_align = align; }

    /// Remove all the buttons
    pub fn clear_buttons(&mut self) { self.buttons.clear(); }

    /// The numerical ID of the current view
    pub fn cur_id(&self) -> Option<usize> { self.tabs.cur_id() }

    /// The current view being shown (the 'focused' view)
    pub fn cur_view(&self) -> Option<&BoxedView> { self.tabs.cur_view() }

    /// Mutable version of `cur_view()`
    pub fn cur_view_mut(&mut self) -> Option<&mut BoxedView> { self.tabs.cur_view_mut() }


    /// The current tab title
    ///
    /// Returns an empty string if the tab dialog is empty
    pub fn cur_title(&self) -> &str {
        if let Some(id) = self.cur_id() {
            for button in &self.title_buttons {
                if let TitleButtonType::Tab(tab_id) = button.b_type {
                    if tab_id == id {
                        return &button.label;
                    }
                }
            }
            ""
        }
        else { "" }
    }
    /// Iterate through all the views in this `TabLayer`
    pub fn views(&self) -> TabIter { self.tabs.views() }

    /// Mutable version of `views()`
    pub fn views_mut(&mut self) -> TabIterMut { self.tabs.views_mut() }

    /// Set the current tab by ID
    pub fn set_cur_tab(&mut self, id: usize) { self.tabs.set_cur_tab(id); }

    /// Set the title of a specific tab id
    pub fn set_title(&mut self, id: usize, new_title: &str) {
        for button in &mut self.title_buttons {
            let upd_title = if let TitleButtonType::Tab(tab_id) = button.b_type { tab_id == id }
            else { false };
            if upd_title { button.set_label(new_title) }
        }
    }

    /// Add a new view tab and returns the ID of the new tab
    pub fn add_tab<V: View>(&mut self, title: &str, view: V) -> usize {
        let new_id = self.tabs.add_tab(view);
        self.set_cur_tab(new_id);
        self.title_buttons.push(TitleButton::new(title, new_id));
        new_id
    }

    /// Add a new view tab
    ///
    /// Chainable version
    #[must_use]
    pub fn tab<V: View>(mut self, title: &str, view: V) -> TabLayer {
        self.add_tab(title, view);
        self.set_cur_tab(0);
        self
    }

    /// Delete tab by ID
    pub fn remove_tab(&mut self, id: usize) {
        self.tabs.remove_tab(id);
        self.title_buttons.retain(|t_bt| {
            if let TitleButtonType::Tab(old_id) = t_bt.b_type {
                old_id != id
            }
            else { true }
        });
    }

    /// Return all the tab IDs
    pub fn ids(&self) -> Vec<usize> { self.tabs.ids() }

    // do we have the next and previous buttons?
    fn has_next_prev_buttons(&self) -> bool {
        matches!(self.buttons.last(), Some(ViewButton { action: ButtonAction::Next, ..}))
    }

    // open the overflow menu
    fn open_overflow(&self) -> EventResult {
        let id_snd = self.id_snd.clone();

        let mut tabs_menu = SelectView::new()
            .on_submit(move |root, id| {
                id_snd.send(*id).unwrap();
                root.pop_layer();
            });

        for button in &self.title_buttons {
            if let TitleButtonType::Tab(tab_id) = button.b_type {
                tabs_menu.add_item(&button.label, tab_id);
            }
        }

        // get overflow button location
        // there should be exactly 4 items if there is an overflow button
        let ofs = self.shown_title_buttons[3].rect.top_left() + (0, 1);

        EventResult::with_cb_once(move |root| {
            // if the user has a custom set FPS, don't mess with it
            // otherwise set the FPS to 30
            if root.fps().is_none() { root.set_fps(30); }

            let current_offset = root
                .screen()
                .layer_offset(LayerPosition::FromFront(0))
                .unwrap_or_else(Vec2::zero);
            let offset = ofs.signed() - current_offset;

            root.screen_mut().add_layer_at(
                Position::parent(offset),
                c_focus!(
                    Dialog::around(tabs_menu.scrollable())
                        .wrap_with(OnEventView::new)
                        .on_event(Event::Key(Key::Esc), |r| { r.pop_layer(); })
                        .max_height(20)
                )
            )
        })
    }

    // execute the action of a selected button
    fn exec_button_action(&mut self, selected: usize) -> EventResult {
        match &self.buttons[selected].action {
            ButtonAction::Close => {
                let tab_num = self.cur_id().unwrap();
                if self.tabs.views().count() > 1 {
                    self.remove_tab(tab_num);
                }
                return EventResult::consumed();
            }

            ButtonAction::Next => self.tabs.next(),
            ButtonAction::Prev => self.tabs.prev(),
            ButtonAction::CallBack(cb) => return EventResult::Consumed(Some(cb.clone()))
        }
        EventResult::consumed()
    }

    // button event handler
    fn button_event(&mut self, event: &Event) -> EventResult {
        let num_title_buttons = self.shown_title_buttons.len();
        let num_buttons = self.buttons.len();

        match event {
            Event::Key(Key::Enter) => {
                match self.focus {
                    TabFocus::TitleBar(selected) => {
                        let button = &self.shown_title_buttons[selected];
                        return match button.b_type {
                            TitleButtonType::Tab(id) => {
                                self.set_cur_tab(id);
                                EventResult::consumed()
                            }

                            TitleButtonType::Overflow => self.open_overflow()
                        }
                    }

                    TabFocus::Buttons(selected) => return self.exec_button_action(selected),
                    TabFocus::Content => { }
                }
            }

            Event::Key(Key::Down) => {
                if matches!(self.focus, TabFocus::TitleBar(_)) {
                    self.focus = TabFocus::Content;
                    if let Ok(result) = self.tabs.take_focus(Direction::none()) {
                        return result.and(EventResult::consumed());
                    }
                }
            }

            Event::Key(Key::Left) => {
                match self.focus {
                    TabFocus::TitleBar(ref mut selected) => {
                        if *selected == 0 { *selected = num_title_buttons; }
                        *selected -= 1;
                        return EventResult::consumed();
                    }

                    TabFocus::Buttons(ref mut selected) => {
                        if *selected == 0 { *selected = num_buttons; }
                        *selected -= 1;
                        return EventResult::consumed();
                    }

                    TabFocus::Content => { }
                }
            }

            Event::Key(Key::Right) => {
                match self.focus {
                    TabFocus::TitleBar(ref mut selected) => {
                        *selected += 1;
                        if *selected >= num_title_buttons { *selected = 0; }
                        return EventResult::consumed();
                    }

                    TabFocus::Buttons(ref mut selected) => {
                        *selected += 1;
                        if *selected >= num_buttons { *selected = 0; }
                        return EventResult::consumed();
                    }

                    TabFocus::Content => { }
                }
            }

            Event::Key(Key::End) => {
                match self.focus {
                    TabFocus::TitleBar(ref mut selected) => {
                        *selected = num_title_buttons - 1;
                        return EventResult::consumed();
                    }

                    TabFocus::Buttons(ref mut selected) => {
                        *selected = num_buttons - 1;
                        return EventResult::consumed();
                    }

                    TabFocus::Content => { }
                }
            }

            Event::Key(Key::Home) => {
                match self.focus {
                    TabFocus::TitleBar(ref mut selected) | TabFocus::Buttons(ref mut selected) => {
                        *selected = 0;
                        return EventResult::consumed();
                    }

                    TabFocus::Content => { }
                }
            }

            Event::Key(Key::Tab) => {
                match self.focus {
                    TabFocus::TitleBar(_) => self.focus = TabFocus::Content,
                    TabFocus::Content => self.focus = TabFocus::Buttons(0),
                    TabFocus::Buttons(ref mut selected) => {
                        if *selected >= self.buttons.len() - 1 { return EventResult::Ignored }
                        *selected += 1;
                    }
                }
                return EventResult::consumed();
            }

            Event::Refresh => {
                if let Ok(id) = self.id_rcv.try_recv() {
                    self.set_cur_tab(id);
                    self.tabs.set_as_first(id);
                    return EventResult::consumed();
                }
            }

            Event::Key(Key::Up) => {
                if matches!(self.focus, TabFocus::Buttons(_)) {
                    self.focus = TabFocus::Content;
                    return EventResult::consumed();
                }
            }

            Event::FocusLost => self.focus = TabFocus::Content,
            _ => { }
        }
        EventResult::Ignored
    }

    // event handler for the currently shown view
    fn view_event(&mut self, event: &Event) -> EventResult {
        match self.tabs.on_event(event.relativized((1, 1))) {
            EventResult::Ignored => {
                match event {
                    Event::Key(Key::Up) => {
                        self.focus = TabFocus::TitleBar(0);
                        return EventResult::consumed().and(self.tabs.on_event(Event::FocusLost));
                    }

                    Event::Key(Key::Down) => {
                        if !self.buttons.is_empty() {
                            self.focus = TabFocus::Buttons(0);
                            return EventResult::consumed().and(self.tabs.on_event(Event::FocusLost));
                        }
                    }

                    Event::Key(Key::Tab) => {
                        if matches!(self.focus, TabFocus::Content) {
                            self.focus = TabFocus::Buttons(0);
                            return EventResult::consumed();
                        }
                    }

                    Event::FocusLost => {
                        self.focus = TabFocus::Content;
                        return EventResult::consumed().and(self.tabs.on_event(Event::FocusLost));
                    }

                    _ => { }
                }
                EventResult::Ignored
            }

            res => res
        }
    }

    // mouse event handler
    fn mouse_event(&mut self, event: &Event) -> Option<EventResult> {
        if let Event::Mouse {
            offset, position,
            event: MouseEvent::Release(button),
        } = event {
            let pos = position.checked_sub(offset)?;

            for (i, t_button) in self.shown_title_buttons.iter().enumerate() {
                if t_button.has_mouse_pos(pos) {
                    return if *button == MouseButton::Left {
                        let res = match t_button.b_type {
                            TitleButtonType::Tab(id) => {
                                self.set_cur_tab(id);
                                EventResult::consumed()
                            }

                            TitleButtonType::Overflow => self.open_overflow()
                        };
                        self.focus = TabFocus::TitleBar(i);
                        Some(res)
                    }
                    else {
                        self.focus = TabFocus::TitleBar(i);
                        Some(EventResult::consumed())
                    }
                }
            }

            for (i, b_button) in self.buttons.iter().enumerate() {
                if b_button.has_mouse_pos(pos) {
                    return if *button == MouseButton::Left {
                        self.focus = TabFocus::Buttons(i);
                        Some(self.exec_button_action(i))
                    }
                    else {
                        self.focus = TabFocus::Buttons(i);
                        Some(EventResult::consumed())
                    }
                }
            }
            self.focus = TabFocus::Content;
        }
        None
    }
}

impl View for TabLayer {
    fn draw(&self, printer: &Printer) {
        // draw the title bar buttons
        if !self.title_buttons.is_empty() {
            let mut f_index: Option<usize> = None;
            for (i, button) in self.shown_title_buttons.iter().enumerate() {
                let selected = if let TabFocus::TitleBar(sel) = self.focus { i == sel && printer.focused }
                else { false };
                let focused = if let TitleButtonType::Tab(id) = button.b_type {
                    if let Some(cur_id) = self.cur_id() {
                        cur_id == id
                    }
                    else { false }
                }
                else { false };
                // skip the focused title button
                if focused { f_index = Some(i); }
                else {
                    button.draw(printer, selected, false, false, false);
                }
            }

            // now draw the focused button so it is on top
            if let Some(index) = f_index {
                let selected = if let TabFocus::TitleBar(sel) = self.focus { index == sel && printer.focused }
                else { false };
                self.shown_title_buttons[index]
                    .draw(printer, selected, true, false, false);
            }
        }
        printer.print_hline((0, 1), printer.size.x, "─");

        let y = if self.buttons.is_empty() { printer.size.y + 1 }
        else { printer.size.y };
        let tabs_printer = printer
            .cropped((printer.size.x, y))
            .offset((0, 2))
            .focused(self.focus == TabFocus::Content);

        self.tabs.draw(&tabs_printer);

        // draw the buttons of there are any
        if !self.buttons.is_empty() {
            for (i, button) in self.buttons.iter().enumerate() {
                let selected = if let TabFocus::Buttons(sel) = self.focus { i == sel && printer.focused }
                else { false };
                button.draw(printer, selected);
            }
        }
    }

    fn layout(&mut self, size: Vec2) {
        // align the title bar buttons
        super::align_title_buttons(&mut self.shown_title_buttons, &self.title_buttons, self.title_align, size.x, false);

        // align the buttons
        super::align_buttons(&mut self.buttons, self.button_align, size, false);

        // set the size for the current view
        self.tabs.layout(size - (0, 3));
    }

    fn take_focus(&mut self, source: Direction) -> Result<EventResult, CannotFocus> {
        let focus_tab = |dialog: &mut Self, d: Direction| {
            let result = dialog.tabs.take_focus(d);
            let focus = if result.is_err() { TabFocus::TitleBar(0) }
            else { TabFocus::Content };
            dialog.focus = focus;
            result
        };

        let mut result: Result<EventResult, CannotFocus> = Ok(EventResult::consumed());
        match source {
            Direction::Abs(Absolute::Down) => result = focus_tab(self, source),

            Direction::Abs(Absolute::Left) | Direction::Abs(Absolute::Right) => {
                if !matches!(self.focus, TabFocus::TitleBar(_)) {
                    result = focus_tab(self, source);
                }
            }

            Direction::Abs(Absolute::Up) => self.focus = TabFocus::TitleBar(0),
            _ => { }
        }

        Ok(result.unwrap_or(EventResult::Ignored))
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        self.mouse_event(&event).unwrap_or(
            if self.focus == TabFocus::Content { self.view_event(&event) }
            else { self.button_event(&event) }
        )
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 { constraint }
    fn needs_relayout(&self) -> bool { self.tabs.needs_relayout() }
    fn focus_view(&mut self, sel: &Selector) -> Result<EventResult, ViewNotFound> { self.tabs.focus_view(sel) }
    fn call_on_any(&mut self, sel: &Selector, cb: AnyCb) { self.tabs.call_on_any(sel, cb); }
}

impl Default for TabLayer {
    fn default() -> TabLayer { TabLayer::new() }
}