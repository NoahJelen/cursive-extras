[![Crates.io](https://img.shields.io/crates/v/cursive-extras)](https://crates.io/crates/cursive-extras)
# Cursive Extras

Extra views for the Cursive TUI library as well some helper functions and macros

## Features
* `image_view`: Enables the `ImageView` view
* `log_view`: Enables the `LogView` view
* `tabs`: Tabbed views similar to `cursive-tabs`
* `advanced`: Advanced views